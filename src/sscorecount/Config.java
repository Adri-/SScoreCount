package sscorecount;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Adri
 */
public class Config {
    public String playerName;
    public int playerId;

    public int gamemode;
    public String api;
    
    public Color bgcolor;
    public Color fontcolor;
    
    public Color positivecolor;
    public Color neutralcolor;
    public Color negativecolor;
    
    public String main;
    public int main_size;
    public String second;
    public int second_size;

    public Config() throws IOException {
        Properties prop = new Properties();
        InputStream input = null;

	try {
        File jarPath=new File(SScoreCount.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath=jarPath.getParentFile().getAbsolutePath();
        System.out.println("Working directory: \n"+propertiesPath);
        prop.load(new FileInputStream(propertiesPath+"/config.properties"));

                this.playerId = Integer.parseInt(prop.getProperty("id"));
                this.gamemode = Integer.parseInt(prop.getProperty("gamemode"));
                this.fontcolor = new Color(     Integer.parseInt(prop.getProperty("font-color").split(",")[0]),
                                                Integer.parseInt(prop.getProperty("font-color").split(",")[1]),
                                                Integer.parseInt(prop.getProperty("font-color").split(",")[2]));
                
                this.bgcolor = new Color(   Integer.parseInt(prop.getProperty("background-color").split(",")[0]),
                                            Integer.parseInt(prop.getProperty("background-color").split(",")[1]),
                                            Integer.parseInt(prop.getProperty("background-color").split(",")[2]));
                
                this.positivecolor = new Color( Integer.parseInt(prop.getProperty("positive-delta").split(",")[0]),
                                                Integer.parseInt(prop.getProperty("positive-delta").split(",")[1]),
                                                Integer.parseInt(prop.getProperty("positive-delta").split(",")[2]));
                
                this.neutralcolor = new Color( Integer.parseInt(prop.getProperty("neutral-delta").split(",")[0]),
                                               Integer.parseInt(prop.getProperty("neutral-delta").split(",")[1]),
                                               Integer.parseInt(prop.getProperty("neutral-delta").split(",")[2]));
                
                this.negativecolor = new Color( Integer.parseInt(prop.getProperty("negative-delta").split(",")[0]),
                                                Integer.parseInt(prop.getProperty("negative-delta").split(",")[1]),
                                                Integer.parseInt(prop.getProperty("negative-delta").split(",")[2]));
                this.main = prop.getProperty("main-font");
                this.second = prop.getProperty("secondary-font");
                this.main_size = Integer.parseInt(prop.getProperty("main-font-size"));
                this.second_size = Integer.parseInt(prop.getProperty("secondary-font-size"));
                
                this.playerName = prop.getProperty("username");
                this.api = prop.getProperty("api-key");
	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

    }
}
