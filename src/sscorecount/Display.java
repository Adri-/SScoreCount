/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sscorecount;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author adrien
 */
public class Display extends javax.swing.JFrame {

    int runss;
    int sscount;
    String score;
    String rank;

    int startrank;
    long startscore;

    Color backgroundColor;
    Color fontColor;

    Color neutralTextColor;
    Color positiveTextColor;
    Color negativeTextColor;

    OsuPlayerRankingFinder read;

    /*
    static int totalPresses;
    static ArrayList<Date> currentPresses;
     */
    /**
     * Creates new form Display
     */
    public Display(OsuPlayerRankingFinder read, Config c) throws IOException {
        this.read = read;
        initComponents();
        init(c);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SSlabel = new javax.swing.JLabel();
        SSsincelabel = new javax.swing.JLabel();
        Scorelabel = new javax.swing.JLabel();
        Ranklabel = new javax.swing.JLabel();
        SSvalue = new javax.swing.JLabel();
        SSsincevalue = new javax.swing.JLabel();
        Scorevalue = new javax.swing.JLabel();
        Rankvalue = new javax.swing.JLabel();
        changeScore = new javax.swing.JLabel();
        changerank = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        SSlabel.setText("Total SS");

        SSsincelabel.setText("Run SS");

        Scorelabel.setText("Score");

        Ranklabel.setText("Rank");

        SSvalue.setText("jLabel5");

        SSsincevalue.setText("jLabel6");

        Scorevalue.setText("jLabel7");

        Rankvalue.setText("jLabel8");

        changeScore.setText("jLabel1");

        changerank.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Ranklabel)
                    .addComponent(Scorelabel)
                    .addComponent(SSsincelabel)
                    .addComponent(SSlabel))
                .addGap(79, 79, 79)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(SSvalue)
                    .addComponent(SSsincevalue)
                    .addComponent(Scorevalue)
                    .addComponent(Rankvalue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(changeScore)
                    .addComponent(changerank))
                .addContainerGap(196, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SSlabel)
                    .addComponent(SSvalue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SSsincelabel)
                    .addComponent(SSsincevalue))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Scorelabel)
                    .addComponent(Scorevalue)
                    .addComponent(changeScore))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Ranklabel)
                    .addComponent(Rankvalue)
                    .addComponent(changerank))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(OsuPlayerRankingFinder read, Config c) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Display.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Display.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Display.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Display.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Display(read, c).setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(Display.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void init(Config c) throws IOException {
        this.setTitle("SScoreCount v0.1");
        
        ImageIcon img = new ImageIcon(getClass().getClassLoader().getResource("resource/images/ssc.png"));
        this.setIconImage(img.getImage());
        this.fetchSettings(c);

        this.getContentPane().setBackground(this.backgroundColor);

        String font = c.main;
        String smallfont = c.second;

        int size = c.main_size;
        int smallsize = c.second_size;

        this.SSlabel.setFont(new java.awt.Font(font, 0, size));
        this.SSlabel.setForeground(this.fontColor);

        this.SSsincelabel.setFont(new java.awt.Font(font, 0, size));
        this.SSsincelabel.setForeground(this.fontColor);

        this.Scorelabel.setFont(new java.awt.Font(font, 0, size));
        this.Scorelabel.setForeground(this.fontColor);

        this.Ranklabel.setFont(new java.awt.Font(font, 0, size));
        this.Ranklabel.setForeground(this.fontColor);

        this.SSvalue.setFont(new java.awt.Font(font, 0, size));
        this.SSvalue.setForeground(this.fontColor);

        this.Scorevalue.setFont(new java.awt.Font(font, 0, size));
        this.Scorevalue.setForeground(this.fontColor);

        this.SSsincevalue.setFont(new java.awt.Font(font, 0, size));
        this.SSsincevalue.setForeground(this.fontColor);

        this.Rankvalue.setFont(new java.awt.Font(font, 0, size));
        this.Rankvalue.setForeground(this.fontColor);

        this.changerank.setFont(new java.awt.Font(smallfont, 0, smallsize));
        this.changeScore.setFont(new java.awt.Font(smallfont, 0, smallsize));

        this.SSvalue.setText("Loading...");
        this.Scorevalue.setText("Loading...");
        this.SSsincevalue.setText("Loading...");
        this.Rankvalue.setText("Loading...");
        this.changerank.setText("");
        this.changeScore.setText("");

        this.runss = 0;
        this.sscount = -1;
    }

    public void refresh(String[] info) throws IOException {
        String rank = "";
        String score = "";
        String SS = "";

        if (info != null) {
            System.out.println("Retrieved information from osu! website");
            rank = info[0];
            score = info[1];
            SS = info[2];
        } else {
            System.out.println("Nothing was found.");
            rank = "You are not in the leaderboard !";
            score = "";
            SS = "";
        }

        System.out.println("Updating displayed values");
        this.Rankvalue.setText(rank);
        this.rank = rank;
        this.Scorevalue.setText(score);
        this.score = score;
        this.SSvalue.setText(SS);

        this.setRunSS(Integer.valueOf(SS.replace(",", "")));
        System.out.println("Retrieval finished.");
    }

    public void refresh(String[] info, boolean first) throws IOException {
        refresh(info);
        if (first) {
            this.startrank = Integer.valueOf(rank.replace("#", ""));
            this.startscore = Long.valueOf(score.replace(",", ""));
        }
        this.setChanges();
    }

    public void setRunSS(int newSS) {
        if (this.sscount == -1) {
            this.sscount = newSS;
            this.runss = 0;
            this.SSsincevalue.setText(Integer.toString(this.runss));
        } else {
            if (this.sscount != newSS) {
                this.runss += newSS - this.sscount;
                this.sscount = newSS;
                this.SSsincevalue.setText(Integer.toString(this.runss));
            }
        }
    }

    public void refreshK() {
        //this.keyTotal.setText(Integer.toString(this.totalPresses));
    }

    public void setChanges() {
        int scoredifference = (int) (Long.valueOf(this.score.replace(",", "")) - this.startscore);
        int rankdifference = (int) (Long.valueOf(this.rank.replace("#", "")) - this.startrank);

        if (scoredifference > 0) {
            this.changeScore.setForeground(this.positiveTextColor);
            this.changeScore.setText("+ " + NumberFormat.getNumberInstance(Locale.US).format(scoredifference));
        } else {
            if (scoredifference == 0) {
                this.changeScore.setForeground(this.neutralTextColor);
                this.changeScore.setText("=");
            }
        }
        if (rankdifference > 0) {
            this.changerank.setForeground(this.negativeTextColor);
            this.changerank.setText("- " + Integer.toString(rankdifference));
        } else {
            if (rankdifference == 0) {
                this.changerank.setForeground(this.neutralTextColor);
                this.changerank.setText("=");
            } else {
                if (rankdifference < 0) {
                    this.changerank.setForeground(this.positiveTextColor);
                    this.changerank.setText("+ " + Integer.toString(Math.abs(rankdifference)));
                }
            }
        }
    }

    public static void addTotalPresses(int n) {
        //Display.totalPresses += n;
    }

    public void fetchSettings(Config c) {
        // Default

        this.backgroundColor = c.bgcolor;
        this.fontColor = c.fontcolor;

        this.neutralTextColor = c.neutralcolor;
        this.positiveTextColor = c.positivecolor;
        this.negativeTextColor = c.negativecolor;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Ranklabel;
    private javax.swing.JLabel Rankvalue;
    private javax.swing.JLabel SSlabel;
    private javax.swing.JLabel SSsincelabel;
    private javax.swing.JLabel SSsincevalue;
    private javax.swing.JLabel SSvalue;
    private javax.swing.JLabel Scorelabel;
    private javax.swing.JLabel Scorevalue;
    private javax.swing.JLabel changeScore;
    private javax.swing.JLabel changerank;
    // End of variables declaration//GEN-END:variables
}
