package sscorecount;

import java.net.*;
import java.io.*;

public class OsuPlayerRankingFinder {

    String url;
    int lastpage;
    int playerId;

    public OsuPlayerRankingFinder() {
    }

    public static void main(String[] args) throws Exception {
    }

    OsuPlayerRankingFinder(Config c) {
        String mod = "osu";
        switch(c.gamemode) {
            case 1:
                mod = "taiko";
                break;
            case 2:
                mod = "fruits";
                break;
            case 3:
                mod = "mania";
        }
        this.playerId = c.playerId;
        this.url = "https://osu.ppy.sh/rankings/" + mod + "/score";
        this.lastpage = 0;
    }

    public String[] findPlayer() throws MalformedURLException, IOException {
        String[] info = null;
        System.out.println("Begin process");

        if (lastpage != 0) {
            info = processPage(lastpage);
            if (info != null) {
                return info;
            } else {
                info = processPage(lastpage - 1);
                if (info != null) {
                    return info;
                } else {
                    info = processPage(lastpage + 1);
                    if (info != null) {
                        return info;
                    }
                }
            }
        }

        for (int page = 0; page <= 20; page++) {
            info = processPage(page);
            if (info != null) {
                return info;
            }
        }
        return null;
    }

    public String[] processPage(int page) throws MalformedURLException, IOException {
        System.out.println("Processing page "+Integer.toString(page));
        String url = this.url + "?page=" + Integer.toString(page);
        // System.out.println("Processing " + url);
        URL webURL = new URL(url);
        URLConnection webCx = webURL.openConnection();
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        webCx.getInputStream()))) {
            String inputLine;

            String last = "";
            String last1 = "";
            String last2 = "";
            String last3 = "";
            while ((inputLine = in.readLine()) != null && !last.contains(Integer.toString(this.playerId))) {
                last3 = last2;
                last2 = last1;
                last1 = last;
                last = inputLine;
            }

            if (last.contains(Integer.toString(this.playerId))) {
                int linecount = 0;

                String rank = last3;
                while (linecount < 16) {
                    inputLine = in.readLine();
                    linecount++;
                }
                String score = inputLine;

                while (linecount < 19) {
                    inputLine = in.readLine();
                    linecount++;
                }
                String ss = inputLine;

                String parsedScore = "";
                boolean toggle = false;
                for (int i = 0; i < score.length(); i++) {
                    char c = score.charAt(i);

                    if (c == '"' || c == '\'') {
                        toggle = !toggle;
                    } else {
                        if (toggle) {
                            parsedScore += c;
                        }
                    }
                }

                String[] info = new String[]{rank, parsedScore, ss};
                this.lastpage = page;
                return info;
            }
        }
        return null;
    }
}
