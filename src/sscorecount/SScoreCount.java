/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sscorecount;

import java.io.IOException;
import static java.lang.Thread.sleep;

/**
 *
 * @author adrien
 */
public class SScoreCount {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        System.out.println("SScoreCount command line\n");
        try {
            Config c = new Config();
            OsuPlayerRankingFinder read = new OsuPlayerRankingFinder(c);
            String[] info = read.findPlayer();
            Display win = new Display(read, c);
            win.refresh(info, true);
            win.show();
            
            while(true) {
                sleep(10000);
                System.out.println("Starting to refresh...");
                info = read.findPlayer();
                win.refresh(info,false);
            }
        } catch (Exception e) {
        }
    }

}
